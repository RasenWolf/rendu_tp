﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Class;

namespace UnitTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void IncrementTest()
        {
            Assert.AreEqual(2, Compteur.Increment(1));
        }

        [TestMethod]
        public void DecremenTest()
        {
            Assert.AreEqual(1, Compteur.Decrement(2));
        }

        [TestMethod]
        public void RazTest()
        {
            Assert.AreEqual(0, Compteur.Raz(125));
        }
    }
}
