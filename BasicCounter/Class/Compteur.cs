﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Class
{

    public class Compteur
    {
        public static int Increment(int chiffre_increment)
        {
            chiffre_increment++;
            return chiffre_increment;
        }

        public static int Decrement(int chiffre_increment)
        {
            chiffre_increment--;
            return chiffre_increment;
        }

        public static int Raz(int chiffre_increment)
        {
            chiffre_increment=0;
            return chiffre_increment;
        }
    }

    
}
